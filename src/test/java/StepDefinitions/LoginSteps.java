package StepDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;

public class LoginSteps {
    WebDriver driver = null;
    String baseUrl = "http://192.168.1.252:8080/auth/realms/flo-realm/protocol/openid-connect/auth?client_id=flo-logic&redirect_uri=http%3A%2F%2Fdocker-flo-qa.bdinfra.net%3A85%2F&state=d9eefc48-8989-4fc3-a571-b4659df8a6c3&response_mode=fragment&response_type=code&scope=openid&nonce=7783cbce-1b7e-480b-ab52-e0655a5c77fb";

    @Before
    public void setUp() {
        String projectPath = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", projectPath + "/src/test/resources/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

    }

    @Given("user goes to the login page")
    public void login_page() throws InterruptedException {
        driver.get(baseUrl);

        Thread.sleep(5000);
    }


    @When("^user inserts (.*) and (.*)$")
    public void insert_data(String username, String password) throws InterruptedException {
        driver.findElement(By.id("username")).sendKeys(username);
        Thread.sleep(1000);
        driver.findElement(By.id("password")).sendKeys(password);
        Thread.sleep(1000);

    }

    @And("user clicks submit")
    public void click_submit() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id='kc-login']")).click();
        Thread.sleep(5000);
    }

    @Then("^user (.*) gets into the welcome page$")
    public void get_into_the_welcome_page(String username) {
        assertTrue(driver.getPageSource().contains(username));
    }

    @After
    public void tearDown() {
        //close browser
        driver.close();
        System.out.println("TEST Completed");
    }
}
